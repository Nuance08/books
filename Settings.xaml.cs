using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Project
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        
        
        public int days { get;set; }
        public List<Book> initial { get; set; }

        public int[] intervals { get; set; }

        public double extraCharge { get; set; }
        public double newCharge { get; set; }

        public Settings()
        {
            InitializeComponent();

            initial = new List<Book>();
            InitialBooks.ItemsSource = initial;
        }        

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(BookAmount.Text) > 0 && Convert.ToInt32(BookAmount.Text) < 1000)
                {
                    InitialBooks.ItemsSource = null;

                    var h1 = (Book)all.SelectedItem;
                    var h2 = h1.CreateCopy();
                    h2.amount = Convert.ToInt32(BookAmount.Text);
                    var flag = true;
                    foreach (Book b in initial)
                    {
                        if ((b.name == h2.name) && (b.authour == h2.authour))
                        {
                            b.amount += h2.amount;
                            flag = false;
                        }
                    }
                    if (flag)
                        initial.Add(h2);

                    InitialBooks.ItemsSource = initial;                    
                }                   
            }
            catch
            {
                MessageBox.Show("Неверный формат ввода");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((N.Text != "") && (exCharge.Text != "") && (NewCharge.Text != "") && (Convert.ToInt32(N.Text) > 0))
                {
                    if ((step1.Text != "") && (step2.Text != "") && (req1.Text != "") && (req2.Text != "") && (Convert.ToInt32(step1.Text) <= Convert.ToInt32(step2.Text)) && (Convert.ToInt32(req1.Text) <= Convert.ToInt32(req2.Text) && (Convert.ToInt32(step1.Text) > 0) && (Convert.ToInt32(step2.Text) > 0) && (Convert.ToInt32(req1.Text) > 0) && (Convert.ToInt32(req2.Text) > 0)))
                    {
                        days = Convert.ToInt32(N.Text);
                        intervals = new int[4];
                        intervals[0] = Convert.ToInt32(step1.Text);
                        intervals[1] = Convert.ToInt32(step2.Text);
                        intervals[2] = Convert.ToInt32(req1.Text);
                        intervals[3] = Convert.ToInt32(req2.Text);
                        extraCharge = Convert.ToInt32(exCharge.Text);
                        newCharge = Convert.ToInt32(NewCharge.Text);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Диапазоны указаны неверно");
                    }
                }
                else
                {
                    MessageBox.Show("Остались незаполненные поля");
                }
            }
            catch
            {
                MessageBox.Show("Неверный формат ввода");
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox1.IsReadOnly = true;
        }              
    }
}
