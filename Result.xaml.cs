using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Project
{
    /// <summary>
    /// Interaction logic for Result.xaml
    /// </summary>
    public partial class Result : Window
    {
        public Result(BookShop shop)
        {
            InitializeComponent();

            Sold.ItemsSource = shop.BooksSold;
            clientsRating.ItemsSource = shop.clients;
            money.Content = Math.Round(shop.currentMoney,2);
            income.Content = Math.Round(shop.totalIncome,2);
            var count = 0;
            foreach (Book b in shop.BooksSold)
                count += b.amount;
            amount.Content = count;           
        }

        private void textbox1_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox1.IsReadOnly = true;
        }

        private void textbox2_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox2.IsReadOnly = true;
        }

        private void textbox3_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox3.IsReadOnly = true;
        }

        private void textbox4_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox4.IsReadOnly = true;
        }

        private void textbox5_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox5.IsReadOnly = true;
        }

        private void textbox6_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox6.IsReadOnly = true;
        }
    }
}
