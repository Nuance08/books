using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace Project
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    delegate void dlg();
    public partial class MainWindow : Window
    {
        public BookShop TheShop;
        List<Book> allBooks = new List<Book>();
        Thread thr;
        dlg _upd;
        int N;
        public MainWindow()
        {
            InitializeComponent();
            
            TheShop = new BookShop();
            using (StreamReader s = new StreamReader("Books.txt", Encoding.Default))
            {
                string str;
                while ((str = s.ReadLine()) != null)
                {
                    string[] line = str.Split(new char[] { '*' }, StringSplitOptions.RemoveEmptyEntries);
                    allBooks.Add(new Book(line[0], line[1], line[2], Convert.ToInt32(line[3]), line[4], Convert.ToInt32(line[5])));
                }
            }
            using (StreamReader s = new StreamReader("Clients.txt", Encoding.Default))
            {
                string str;
                while ((str = s.ReadLine()) != null)
                {
                    string[] line = str.Split(new char[] { '*' }, StringSplitOptions.RemoveEmptyEntries);
                    TheShop.clients.Add(new Client(line[0], line[1]));
                }
            }
            foreach (Book b in allBooks)
            {
                var flag = false;
                foreach(Publisher p in TheShop.publishers)
                {
                    if (b.publisher == p.name)
                    {
                        flag = true;
                        var flag2 = false;
                        foreach (Book pb in p.books)
                            if ((pb.name == b.name) && (pb.authour == b.authour))
                            {
                                flag2 = true;
                            }
                        if (!(flag2))
                            p.books.Add(b);
                    }
                }
                if (!(flag))
                    TheShop.publishers.Add(new Publisher(b.publisher, b));
            }
            Settings set = new Settings();
            set.all.ItemsSource = allBooks;
            set.ShowDialog();
            N = set.days;
            TheShop.avalible = set.initial;
            TheShop.extraCharge = set.extraCharge;
            TheShop.newCharge = set.newCharge;
            TheShop.intervals = set.intervals;
            
            _upd = Update;
            Dispatcher.BeginInvoke(_upd);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if ((string)MainButton.Content == "Начать")
            {
                thr = new Thread(Contin);
                thr.Start();
                MainButton.Content = "Пауза";
                TheShop.log.Push("Пауза, проверяй");
            }
            else
            {
                if ((string)MainButton.Content != "Пауза")
                {
                    MainButton.Content = "Пауза";
                    thr.Resume();
                }
                else
                {
                    MainButton.Content = "Продолжить";
                    thr.Suspend();
                }
            }
            
        }

        private void Update()
        {
            Orders.ItemsSource = null;
            LogPanel.ItemsSource = null;
            BooksRequested.ItemsSource = null;
            BooksAvalible.ItemsSource = null;
            MoneyCounter.Content = null;
            DayCounter.Content = null;
            
            MoneyCounter.Content = Math.Round(TheShop.currentMoney,2);
            DayCounter.Content = TheShop.days;
            BooksAvalible.ItemsSource = TheShop.avalible;
            BooksRequested.ItemsSource = TheShop.requests;
            Orders.ItemsSource = TheShop.pending;
            LogPanel.ItemsSource = TheShop.log;
        }
        private void Contin()
        {
            while (TheShop.days < N)
            {
                    TheShop.update();
                    int k;
                    if (TheShop.avalible.Count < allBooks.Count / 3)
                        k = new Random().Next(1, 2);
                    else
                    {
                        if (TheShop.avalible.Count < allBooks.Count / 2)
                            k = new Random().Next(2, 3);
                        else
                            k = new Random().Next(3, 4);
                    }
                    for (int check = 0; check < k; check++)
                    {
                        var r = new Random();
                        var i = r.Next(1, 5);
                        var s = TheShop.clients[r.Next(0, TheShop.clients.Count)];
                        var lst = new List<Book>();
                        while (lst.Count < i)
                        {
                            var flag = true;
                            var b = allBooks[r.Next(0, allBooks.Count)];
                            foreach (Book l in lst)
                                if ((l.name == b.name) && (l.authour == b.authour))
                                    flag = false;
                            if (flag)
                                lst.Add(b);
                        }
                        foreach (Book b in lst)
                            b.amount = r.Next(1, 6);
                        var o = new Order(s.name, s.contactInfo, lst);
                        TheShop.AddOrder(o);
                        Dispatcher.BeginInvoke(_upd);
                        Thread.Sleep(500);
                    }
                    Thread.Sleep(1000);
            }
            
            if (TheShop.days >= N)
            {
                Dispatcher.BeginInvoke(_upd);
                Dispatcher.BeginInvoke(new Action(delegate()
                {
                        Result r = new Result(TheShop);
                        r.ShowDialog();
                    Close();                    
                }));
            }
        }

        private void textbox1_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox1.IsReadOnly = true;            
        }

        private void textbox2_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox2.IsReadOnly = true;
        }

        private void textbox3_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox3.IsReadOnly = true;
        }

        private void textbox4_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox4.IsReadOnly = true;
        }

        private void textbox5_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox5.IsReadOnly = true;
        }

        private void textbox6_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox6.IsReadOnly = true;
        }

        private void textbox7_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox7.IsReadOnly = true;
        }

        private void textbox8_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox8.IsReadOnly = true;
        }

        private void textbox9_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox9.IsReadOnly = true;
        }

        private void textbox10_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox10.IsReadOnly = true;
        }

        private void textbox11_TextChanged(object sender, TextChangedEventArgs e)
        {
            textbox11.IsReadOnly = true;
        }
    }
}
