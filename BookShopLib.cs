using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
  public class BookShop
    {
        public int days { get; set; }
        //public double money { get; set; }
        public List<Publisher> publishers { get; set; }
        public List<Book> avalible { get; set; }
        public List<Request> requests { get; set; }
        public List<Order> pending { get; set; }
        public Stack<string> log { get; set; }

        public BookShop()
        {
            avalible = new List<Book>();
            requests = new List<Request>();
            log = new Stack<string>();
            log.Push("Start!");
            pending = new List<Order>();
            publishers = new List<Publisher>();
            days = 1;
            //money = 5000;
        }
        
         public void update()
        {
            requests.RemoveAll(q => q.days == 0);
            log.Push("Here comes day number " + days + "!");
            days++;
            CheckRequests();
            CheckOrders();
            CheckBooks();
        }

        public void CheckRequests()
        {
            foreach (Request r in requests)
            {
                r.days--;
                if (r.days == 0)
                {
                    foreach (Book b in r.list)
                    {
                        log.Push("We recieved " + b.amount + " copies of " + b.name + ", requested from " + r.publisher);
                        var flag = true;
                        foreach (Book a in avalible)
                            if ((a.name == b.name) && (a.authour == b.authour))
                            {
                                a.amount += b.amount;
                                flag = false;
                            }
                        if (flag)
                            avalible.Add(b);
                        /*
                        if (avalible.Find(q => q == b) != null)
                        {
                            Book a = avalible.Find(q => q == b);
                            a.amount += b.amount;
                            avalible.Remove(b);
                            avalible.Add(a);
                        }
                        else
                            avalible.Add(b);
                        */
                    }
                }
            }
            requests.RemoveAll(q => q.days <= 0);
        }
        public void CheckOrders()
        {
            foreach (Book a in avalible)
            {
                foreach (Order o in pending)
                {
                    o.list.RemoveAll(q => q.amount == 0);
                    foreach (Book b in o.list)
                        if ((a.name == b.name) && (a.authour == b.authour) && (a.amount != 0) && (b.amount != 0) && (a.amount >= b.amount))
                        {
                            log.Push(o.customerInfo + " recieved " + b.amount + " copies of " + a.authour + "'s '" + a.name + "'");
                            //money += (a.cost + a.extraCharge) * b.amount;
                            //log.Push("Bookshop recieved " + (a.cost + a.extraCharge) * b.amount + "$, a payment from " + o.customerInfo);
                            a.amount -= b.amount;
                            b.amount = 0;
                        }
                    o.list.RemoveAll(q => q.amount == 0);
                }
                pending.RemoveAll(q => q.list.Count < 1);
                requests.RemoveAll(q => q.list.Count == 0);
            }
        }
        public void CheckBooks()
        {
            foreach (Publisher p in publishers)
            {
                var req = new List<Book>();
                foreach (Book b in avalible)
                    if ((p.name == b.publisher) && (b.amount < 3))
                    {
                        req.Add(b);
                    }
                foreach (Book b in req)
                {
                    b.amount = 5;
                    log.Push("We run low on " + b.authour + "'s '" + b.name + "', requesting extra 5 copies from " + b.publisher);
                    //money -= b.amount * b.cost;
                }
                if (req.Count != 0)
                    requests.Add(new Request(p.name, req));
            }
        }
        public void AddOrder(Order o)
        {
            log.Push("We have new order from " + o.customerInfo);

            foreach (Book a in avalible)
                foreach (Book b in o.list)
                    if ((a.name == b.name) && (a.authour == b.authour) && (a.amount >= b.amount) && (a.amount != 0) && (b.amount != 0))
                    {
                        log.Push(o.customerInfo + " recieved " + b.amount + " copies of " + a.authour + "'s '" + a.name + "'");
                        //money += (a.cost + a.extraCharge) * b.amount;
                        //log.Push("Bookshop recieved " + (a.cost + a.extraCharge) * b.amount + "$, a payment from " + o.customerInfo);
                        a.amount -= b.amount;
                        b.amount = 0;
                    }
            o.list.RemoveAll(q => q.amount == 0);

            if (o.list.Count != 0)
            {
                foreach (Publisher p in publishers)
                {
                    var req = new List<Book>();
                    foreach (Book b in o.list)
                    {
                        foreach (Book pb in p.books)
                        {
                            if ((b.name == pb.name) && (b.authour == pb.authour))
                            {
                                req.Add(b);
                                //money -= pb.amount * pb.cost;
                            }
                        }
                    }
                    if (req.Count != 0)
                    {
                        requests.Add(new Request(p.name, req));
                        req = null;
                    }
                }
                foreach (Book b in o.list)
                {
                    log.Push(o.customerInfo + " ordered '" + b.name + "'," + " " + b.authour);
                }
                pending.Add(o);
            }
        }
        public class Book
    {
        public string name { get; set; }
        public string authour { get; set; }
        public string category { get; set; }
        public int numberOfPages { get; set; }
        public string publisher { get; set; }

        public int amount { get; set; }
        public double cost { get; set; }
        public int rating { get; set; }
        public double extraCharge
        {
            get
            {
                if (isNew)
                    return cost / 2;
                else
                    return cost / 4;
            }
        }

        public bool isNew { get; set; }

        public Book(string s1, string s2, int i)
        {
            authour = s1;
            name = s2;
            amount = i;
        }
        public Book(string s1, int i)
        {
            authour = s1;
            isNew = true;
            amount = i;
        }
        public Book(string s1, string s2, string s3, int i1, string s4, int i2)
        {
            authour = s1;
            name = s2;
            category = s3;
            numberOfPages = i1;
            publisher = s4;
            cost = i2;
            amount = 0;
            rating = 0;
            isNew = true;
        }
        public class Publisher
    {
        public string name { get; set; }
        public List<Book> books { get; set; }

        public Publisher(string s, Book b)
        {
            name = s;
            books = new List<Book>();
            books.Add(b);
        }

        public void AddBook(Book b)
        {
            books.Add(b);
        }
        public void RemoveBook(Book b)
        {
            books.Remove(b);
        }
    }
    public class Order
    {
        public string customerInfo { get; set; }
        public string contactInfo { get; set; }
        public List<Book> list { get; set; }
        
        public Order(string s1, string s2, List<Book> l)
        {
            customerInfo = s1;
            contactInfo = s2;
            list = l;
        }
        public Order(string s1, string s2)
        {
            customerInfo = s1;
            contactInfo = s2;
            list = new List<Book>();
        }
        public void AddBook(Book b, int i)
        {
            b.amount = i;
            list.Add(b);
        }
        public void RemoveBook(Book b)
        {
            list.Remove(b);
        }
    }
    public class Request
    {
        public string publisher { get; set; }
        public int days { get; set; }
        public List<Book> list { get; set; }

        public Request(string s, List<Book> l)
        {
            var r = new Random();
            days = r.Next(1, 6);
            publisher = s;
            list = l;
        }
    }
    }
    }